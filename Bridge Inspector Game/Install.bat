@echo OFF

reg Query "HKLM\Hardware\Description\System\CentralProcessor\0" | find /i "x86" > NUL && set OS=32BIT || set OS=64BIT

if %OS%==32BIT start "Installer" ./32-bit/Engine/Extras/Redist/en-us/UE4PrereqSetup_x86.exe
if %OS%==64BIT start "Installer" ./64-bit/Engine/Extras/Redist/en-us/UE4PrereqSetup_x64.exe

