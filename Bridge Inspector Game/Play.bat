@echo OFF

reg Query "HKLM\Hardware\Description\System\CentralProcessor\0" | find /i "x86" > NUL && set OS=32BIT || set OS=64BIT

if %OS%==32BIT start "Bridge Inspector" ./32-bit/Bridge_Inspector.exe
if %OS%==64BIT start "Bridge Inspector" ./64-bit/Bridge_Inspector.exe

